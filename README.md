# cooperative.studio

a new creative studio for a new media landscape

a digital workplace dedicated to dignity and accountability through distributed ownership and democratic decision-making


## goals

### no more sociopathy in employment

in pandemic world, millions are without work across dozens of industries and firms are either maladpting or collapsing when faced with the requirements of digitizing their workplace; many of those maladaptations include increasing alienation and anxiety imposed on employees and recruits. a component of that experience is the desire to continue to exert and develop new mechanisms of workplace control on behalf of ownership, including: unpaid labor, unclear job expectations, reductions in personal control over process responsibilities, and further atomization in the production of deliverables. where the employee/employer relationship was already fully digitized, many job seekers in media and technology roles are experiencing these *before the hiring process even begins*. for 200 years, socialists have been theorizing solutions to these and hosts of other workplace problems; none present opportunities for absolute solvency, but the cooperative model with transparent, clear, and open democratic processes spread across many diverse assemblies and problem areas has been observed by successful model for maximizing firm cohesion through decision-making transparency.


### propaganda is dead, long live propaganda

no cohort appears better at "is it art?" || "is it effective?" rhetorical critique than the so called 'online left'; identifying and calling out state propaganda to flipping and reversing bourgeois messaging is critical to developing messaging and narratives that "slide home like a bolt" alongside of all manner of organizations. from unions organizing membership drives to disaster recovery volunteers organizing rapid fundraising for supplies and transport to political campaigns, there is a real failure to connect with 'weird twitter' because the marketers that control the media landscape at the end of the beginning of 2021 have simply failed to try to shape their messaging to a more materialist, more universalist, and more consistent ideological bloc. bringing these perspectives and horizontalist structures into a firm could be a powerful motor for new kinds of educational, political, organizational and (yes) marketing rhetoric. 


### democratic creative and client management

the traditional "named partner and his project manager run the show" structure has failed an uncountable number of firms that need to spread their message to stay relevant, while accumulating untold wealth for legacy firms. that monolith, where a single minded "quality filter" only weighs in with a *pollice verso*, defends itself using only the adage "a camel is a horse designed by committee" and variants. this ignores 2 important considerations for decision-making in creative processes: 

 1. the mimetic content production engine online proves that bangers **absolutely** can come from consensus. in fact, more often than not the latest memes you saved to your device will be products of a highly iterative and decentralized process.
 1. the 'man upstairs' who determines if a creative product "passes muster", and the small coterie of taste-makers who "know what works for him" operate to deprive people who do the labor in creative industries of agency and control over their work product, and contribute to the feelings of anxiety, precarity, and alienation that stem from an unpredictable and potentially hostile workplace

unsurprisingly, a similar set of problems arise when the choices, or frameworks for decision-making, of clients to pursue are tightly controlled by a few "senior" managers: clients become socially regarded as "belonging" to those individuals the managers individually choose to acknowledge without any oversight, conferring power to an increasingly centralized set of teams, and potentially reducing earning and promotional opportunities for all others. organizing and voting on sales decision-making provides opportunities for creating and later improving upon frameworks - and reduces the firms entire reliance on the networking capacities of any single individual.


## todo

### backend 

#### infrastructure

coop will avoid where possible sharing revenues with Amazon. 

coop will endeavor to make all vendor and supplier choices through democratic process.

coop will keep a log of all instances of democratic process, and ideally a democratically decided charter that when amended is stored in version control and automatically updated without ever being writable by a person (amendments should modify text in 'the following way' - only code can write to the charter).

#### api

coop will develop a flagship in-house api that serves as external marketing, client portal, organizational decision-making, and project management platform.

 - develop as much in-house as possible, using open source technologies 
 - bearer user authentication for relevant roles
    - administrator
    - employee
    - assembly representative
    - assembly member
    - client manager
    - client stakeholder
    - client representative
 - democratic decision-making
    - assembly
        - representative elections
        - proposals
        - discussions
        - votes
    - client management
        - representative communications
        - feedback
 - communications
    - internal
        - IRC - real time communications
        - posts - pitch development, assembly proposals, internal documentation
        - api bug reports
    - client
        - deliverable bug reports
        - QA plans
        - initial creative
        - version control on revisions (automate as much as possible)
        - invoicing reporting


#### automation

automation is an absolute necessity for coop success in a highly competitive environment
 - coop charter management
 - invoice generation and notification
 - pitch deck generation
 - visual elements filtration


### frontend

