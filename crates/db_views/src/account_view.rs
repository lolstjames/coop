use diesel::{result::Error, *};
use db_queries::{ToSafe, ToSafeSettings};
use db_schema::{
    schema::{accounts,persons},
    bindings::{
        person::{Person,PersonSafe},
        account::{Account,AccountSettings},
    },
    PersonId,
    AccountId,
};
use serde::Serialize;

#[derive(Debug, Serialize, Clone)]
pub struct AccountView {
    pub account: Account,
    pub person: Person,
    // TODO: Person Stats
}

type AccountViewTuple = (Account, Person);

impl AccountView {
    pub fn read(conn: &PgConnection, account_id: AccountId) -> Result<Self, Error> {
        let (account, person) = accounts::table
            .find(account_id)
            .inner_join(persons::table)
            .select((
                accounts::all_columns,
                persons::all_columns,
            ))
            .first::<AccountViewTuple>(conn)?;
        Ok(Self{
            account,
            person,
        })
    }

    pub fn read_person(conn: &PgConnection, person_id: PersonId) -> Result<Self, Error> {
        let (account, person) = accounts::table
            .filter(persons::id.eq(person_id))
            .inner_join(persons::table)
            .select((
                accounts::all_columns,
                persons::all_columns,
            ))
            .first::<AccountViewTuple>(conn)?;
        Ok(Self {
            account,
            person,
        })
    }
    
    pub fn find_by_email(conn: &PgConnection, search_query: &str) -> Result<Self, Error> {
        let (account, person) = accounts::table
            .inner_join(persons::table)
            .filter(persons::email.eq(search_query))
            .select((
                accounts::all_columns,
                persons::all_columns,
            ))
            .first::<AccountViewTuple>(conn)?;
        Ok(Self {
            account,
            person,
        })
    }

    pub fn find_by_name(conn: &PgConnection, search_query: &str) -> Result<Self, Error> {
        let (account, person) = accounts::table
            .inner_join(persons::table)
            .filter(persons::name.eq(search_query))
            .select((
                accounts::all_columns,
                persons::all_columns,
            ))
            .first::<AccountViewTuple>(conn)?;
        Ok(Self {
            account,
            person,
        })
    }

    pub fn find_by_email_or_name(conn: &PgConnection, search_query: &str) -> Result<Self, Error> {
        let (account, person) = accounts::table
            .inner_join(persons::table)
            .filter(persons::name.ilike(search_query).or(persons::email.ilike(search_query)))
            .select((
                accounts::all_columns,
                persons::all_columns,
            ))
            .first::<AccountViewTuple>(conn)?;
        Ok(Self {
            account,
            person,
        })
    }
}

#[derive(Debug, Serialize, Clone)]
pub struct AccountSettingsView {
    pub account: AccountSettings,
    pub person: PersonSafe,
    // TODO: Person Stats
}

type AccountSettingsViewTuple = (AccountSettings, PersonSafe);

impl AccountSettingsView {
    pub fn read(conn: &PgConnection, account_id: AccountId) -> Result<Self, Error> {
      let (account, person) = accounts::table
        .find(account_id)
        .inner_join(persons::table)
        .select((
          Account::safe_settings_columns_tuple(),
          Person::safe_columns_tuple(),
        ))
        .first::<AccountSettingsViewTuple>(conn)?;
      Ok(Self {
        account,
        person,
      })
    }
  }
  