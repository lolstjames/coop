use db_queries::{
    ToSafeSettings,
    ViewToVec,
    MaybeOptional,
};
use db_schema::{
    bindings::{
        post::Post,
        account::{Account,AccountSettings},
    },
    schema::{accounts, liked_posts, posts},
    AccountId,
    PostId,
};
use diesel::{pg::Pg, result::Error, *};
use log::debug;
use serde::Serialize;

#[derive(Debug, PartialEq, Serialize, Clone)]
pub struct PostView {
    pub post: Post,
    pub creator: AccountSettings,
}

type PostViewTuple = (
    Post,
    AccountSettings,
);

impl PostView {
    pub fn read(
        conn: &PgConnection,
        post_id: PostId,
        my_account_id: Option<AccountId>,
    ) -> Result<Self, Error> {
//        let account_id_join = my_account_id.unwrap_or(AccountId(-1));

        let (
            post,
            creator,
        ) = posts::table
            .find(post_id)
            .inner_join(accounts::table)
            .select((
                posts::all_columns,
                Account::safe_settings_columns_tuple(),                
            ))
            .first::<PostViewTuple>(conn)?;

        Ok(PostView {
            post,
            creator,
        })
    }
}

impl ViewToVec for PostView {
    type DbTuple = PostViewTuple;

    fn from_tuple_to_vec(items: Vec<Self::DbTuple>) -> Vec<Self> {
        items
            .iter()
            .map(|p| Self{
                post: p.0.to_owned(),
                creator: p.1.to_owned()
            })
            .collect::<Vec<Self>>()
    }
}