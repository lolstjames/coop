use crate::{
    schema::{liked_posts, posts, read_posts, saved_posts},
    AccountId,
    AssemblyId,
    ClientId,
    DbUrl,
    DeliverableId,
    PostId,
    ProjectId,
};
use serde::Serialize;

#[derive(Clone, Queryable, Identifiable, PartialEq, Debug, Serialize)]
#[table_name = "posts"]
pub struct Post {
    pub id: PostId,
    pub title: String,
    pub url: Option<DbUrl>,
    pub body: Option<String>,
    pub creator_id: AccountId,
    pub project_id: Option<ProjectId>,
    pub client_id: Option<ClientId>,
    pub deliverable_id: Option<DeliverableId>,
    pub assembly_id: Option<AssemblyId>,
    pub visibility: Option<i32>,
    pub removed: bool,
    pub locked: bool,
    pub created: chrono::NaiveDateTime,
    pub updated: Option<chrono::NaiveDateTime>,
}

#[derive(Insertable, AsChangeset, Default)]
#[table_name = "posts"]
pub struct PostForm {
    pub title: String,
    pub creator_id: AccountId,
    pub url: Option<DbUrl>,
    pub body: Option<String>,
    pub removed: Option<bool>,
    pub locked: Option<bool>,
}

#[derive(Identifiable, Queryable, Associations, PartialEq, Debug)]
#[belongs_to(Post)]
#[table_name = "liked_posts"]
pub struct PostFav {
    pub id: i32,
    pub post_id: PostId,
    pub account_id: AccountId,
    pub score: i16,
    pub created: chrono::NaiveDateTime,
    pub updated: Option<chrono::NaiveDateTime>,
}

#[derive(Insertable, AsChangeset, Clone)]
#[table_name = "liked_posts"]
pub struct PostFavForm {
    pub post_id: PostId,
    pub account_id: AccountId,
    pub score: i16,
}
