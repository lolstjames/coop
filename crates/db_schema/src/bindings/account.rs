use crate::{schema::accounts, AccountId, PersonId};
use serde::Serialize;
use chrono::NaiveDateTime;

#[derive(Clone, Queryable, Identifiable, PartialEq, Debug, Serialize)]
#[table_name = "accounts"]
pub struct Account {
    pub id: AccountId,
    pub person_id: PersonId,
    pub username: String,
    pub passhash: String,
    pub handle: String, 
    pub password_reset: Option<String>,
    pub password_reset_expires: Option<chrono::NaiveDateTime>, 
    pub public_key: Option<String>,
    pub private_key: Option<String>,
    pub no_contact: Option<bool>,
    pub logins: Option<i16>,
    pub last_login: Option<chrono::NaiveDateTime>,
    pub deleted: Option<bool>,
    pub created: chrono::NaiveDateTime,
    pub updated: Option<chrono::NaiveDateTime>,
}

#[derive(Clone, Insertable, AsChangeset, Default)]
#[table_name = "accounts" ]
pub struct AccountForm {
    pub person_id: PersonId,
    pub passhash: String,
    pub username: String,
    pub handle: String, 
    pub public_key: Option<String>,
    pub private_key: Option<String>,
    pub no_contact: bool,
    pub deleted: Option<bool>,
}

#[derive(Clone, Queryable, Identifiable, PartialEq, Debug, Serialize)]
#[table_name = "accounts"]
pub struct AccountSettings {
    pub id: AccountId,
    pub person_id: PersonId,
    pub username: String,
    pub handle: String,
    pub no_contact: Option<bool>,
    pub public_key: Option<String>,
    pub deleted: Option<bool>,
    pub created: chrono::NaiveDateTime,
    pub updated: Option<chrono::NaiveDateTime>,
}