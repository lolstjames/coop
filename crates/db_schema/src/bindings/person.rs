use crate::{
    schema::persons,
    PersonId,
};
use serde::Serialize;

#[derive(Clone, Queryable, Identifiable, PartialEq, Debug, Serialize)]
#[table_name = "persons"]
pub struct Person {
    pub id: PersonId,
    pub name: String,
    pub email: String,
    pub bio: Option<String>,
    pub pronouns: Option<String>,
    pub phone: Option<String>,
    pub provider: Option<String>,
    pub street: Option<String>,
    pub city: Option<String>,
    pub state: Option<String>,
    pub zip: Option<String>,
    pub country: Option<String>,
    pub deleted: Option<bool>,
    pub created: chrono::NaiveDateTime,
    pub updated: Option<chrono::NaiveDateTime>,
}
#[derive(Insertable, AsChangeset, Clone, Default)]
#[table_name = "persons"]
pub struct PersonForm {
    pub name: String,
    pub email: String,
    pub bio: Option<String>,
    pub pronouns: Option<String>,
    pub phone: Option<String>,
    pub provider: Option<String>,
    pub street: Option<String>,
    pub city: Option<String>,
    pub state: Option<String>,
    pub zip: Option<String>,
    pub country: Option<String>,
    pub deleted: Option<bool>,
}

#[derive(Clone, Queryable, Identifiable, PartialEq, Debug, Serialize)]
#[table_name = "persons"]
pub struct PersonSafe {
    pub id: PersonId,
    pub name: String,
    pub bio: Option<String>,
    pub pronouns: Option<String>,
    pub phone: Option<String>,
    pub provider: Option<String>,
    pub street: Option<String>,
    pub city: Option<String>,
    pub state: Option<String>,
    pub zip: Option<String>,
    pub country: Option<String>,
    pub deleted: Option<bool>,
    pub created: chrono::NaiveDateTime,
}

