table! {
    accounts (id) {
        id -> Int4,
        person_id -> Int4,
        username -> Varchar,
        passhash -> Text,
        handle -> Varchar,
        password_reset -> Nullable<Varchar>,
        password_reset_expires -> Nullable<Timestamp>,
        public_key -> Nullable<Text>,
        private_key -> Nullable<Text>,
        no_contact -> Nullable<Bool>,
        logins -> Nullable<Int2>,
        last_login -> Nullable<Timestamp>,
        deleted -> Nullable<Bool>,
        created -> Timestamp,
        updated -> Nullable<Timestamp>,
    }
}

table! {
    approvals (id) {
        id -> Int4,
        value -> Nullable<Int4>,
        account_id -> Int4,
        assembly_id -> Nullable<Int4>,
        client_id -> Nullable<Int4>,
        project_id -> Nullable<Int4>,
        deliverable_id -> Nullable<Int4>,
        proposal_id -> Nullable<Int4>,
        created -> Timestamp,
        updated -> Nullable<Timestamp>,
    }
}

table! {
    assemblies (id) {
        id -> Int4,
        name -> Varchar,
        domain -> Text,
        proposed_by -> Int4,
        created -> Timestamp,
        updated -> Nullable<Timestamp>,
    }
}

table! {
    clients (id) {
        id -> Int4,
        name -> Varchar,
        logo -> Nullable<Bytea>,
        contact_name -> Nullable<Varchar>,
        contact_email -> Nullable<Varchar>,
        contact_phone -> Nullable<Varchar>,
        contact_provider -> Nullable<Varchar>,
        phone -> Nullable<Varchar>,
        email -> Nullable<Varchar>,
        street -> Nullable<Varchar>,
        city -> Nullable<Varchar>,
        state -> Nullable<Varchar>,
        country -> Nullable<Bpchar>,
        zip -> Nullable<Varchar>,
        creator_id -> Nullable<Int4>,
        created -> Timestamp,
        updated -> Nullable<Timestamp>,
    }
}

table! {
    deliverables (id) {
        id -> Int4,
        title -> Varchar,
        description -> Nullable<Text>,
        project_id -> Int4,
        deleted -> Nullable<Bool>,
        visibility -> Nullable<Int4>,
        creator_id -> Nullable<Int4>,
        created -> Timestamp,
        updated -> Nullable<Timestamp>,
    }
}

table! {
    liked_posts (id) {
        id -> Int4,
        post_id -> Int4,
        account_id -> Int4,
        score -> Int2,
        created -> Timestamp,
        updated -> Nullable<Timestamp>,
    }
}

table! {
    persons (id) {
        id -> Int4,
        name -> Varchar,
        email -> Varchar,
        bio -> Nullable<Text>,
        pronouns -> Nullable<Varchar>,
        phone -> Nullable<Varchar>,
        provider -> Nullable<Varchar>,
        street -> Nullable<Varchar>,
        city -> Nullable<Varchar>,
        state -> Nullable<Varchar>,
        country -> Nullable<Bpchar>,
        zip -> Nullable<Varchar>,
        deleted -> Nullable<Bool>,
        created -> Timestamp,
        updated -> Nullable<Timestamp>,
    }
}

table! {
    posts (id) {
        id -> Int4,
        title -> Varchar,
        url -> Nullable<Text>,
        body -> Nullable<Text>,
        creator_id -> Int4,
        project_id -> Nullable<Int4>,
        client_id -> Nullable<Int4>,
        deliverable_id -> Nullable<Int4>,
        assembly_id -> Nullable<Int4>,
        visibility -> Nullable<Int4>,
        removed -> Bool,
        locked -> Bool,
        created -> Timestamp,
        updated -> Nullable<Timestamp>,
    }
}

table! {
    projects (id) {
        id -> Int4,
        title -> Varchar,
        description -> Nullable<Text>,
        client_id -> Int4,
        deleted -> Nullable<Bool>,
        visibility -> Nullable<Int4>,
        creator_id -> Nullable<Int4>,
        created -> Timestamp,
        updated -> Nullable<Timestamp>,
    }
}

table! {
    read_posts (id) {
        id -> Int4,
        post_id -> Int4,
        account_id -> Int4,
        created -> Timestamp,
        updated -> Nullable<Timestamp>,
    }
}

table! {
    saved_posts (id) {
        id -> Int4,
        post_id -> Int4,
        account_id -> Int4,
        created -> Timestamp,
        updated -> Nullable<Timestamp>,
    }
}

joinable!(accounts -> persons (person_id));
joinable!(approvals -> accounts (account_id));
joinable!(clients -> accounts (creator_id));
joinable!(deliverables -> accounts (creator_id));
joinable!(deliverables -> projects (project_id));
joinable!(liked_posts -> accounts (account_id));
joinable!(liked_posts -> posts (post_id));
joinable!(posts -> accounts (creator_id));
joinable!(posts -> assemblies (assembly_id));
joinable!(posts -> clients (client_id));
joinable!(posts -> deliverables (deliverable_id));
joinable!(posts -> projects (project_id));
joinable!(projects -> accounts (creator_id));
joinable!(projects -> clients (client_id));
joinable!(read_posts -> accounts (account_id));
joinable!(read_posts -> posts (post_id));
joinable!(saved_posts -> accounts (account_id));
joinable!(saved_posts -> posts (post_id));

allow_tables_to_appear_in_same_query!(
    accounts,
    approvals,
    assemblies,
    clients,
    deliverables,
    liked_posts,
    persons,
    posts,
    projects,
    read_posts,
    saved_posts,
);
