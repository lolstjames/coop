use db_queries::{DbPool, CRUD};
use db_schema::{bindings::post::Post, AccountId, PostId};
use db_views::account_view::{AccountView,AccountSettingsView};
use diesel::PgConnection;
use log::error;
use serde::{Deserialize, Serialize};
use url::Url;
use utils::{
    claims::Claims,
    settings::structs::Settings, 
    ApiError, 
    CoopError,
};

pub mod post;
pub mod account;

pub async fn blocking<F, T>(pool: &DbPool, f: F) -> Result<T, CoopError>
where
    F: FnOnce(&diesel::PgConnection) -> T + Send + 'static,
    T: Send + 'static,
{
    let pool = pool.clone();
    let res = actix_web::web::block(move || {
        let conn = pool.get()?;
        let res = (f)(&conn);
        Ok(res) as Result<_, CoopError>
    })
    .await?;
    Ok(res)
}

pub async fn get_account_view_from_jwt(
    jwt: &str,
    pool: &DbPool,
) -> Result<AccountView, CoopError> {
    let claims = Claims::decode(&jwt)
        .map_err(|_| ApiError::err("not_logged_in"))?
        .claims;
    let account_id = AccountId(claims.sub);
    let account_view = 
        blocking(pool, move |conn| AccountView::read(conn, account_id)).await??;

    Ok(account_view)
}

pub async fn get_account_view_from_jwt_opt(
    jwt: &Option<String>,
    pool: &DbPool,
) -> Result<Option<AccountView>, CoopError> {
    match jwt {
        Some(jwt) => Ok(Some(get_account_view_from_jwt(jwt, pool).await?)),
        None => Ok(None),
    }
}

/// Checks the password length
pub fn password_length_check(pass: &str) -> Result<(), CoopError> {
    if pass.len() > 60 {
        Err(ApiError::err("invalid_password").into())
    } else {
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
