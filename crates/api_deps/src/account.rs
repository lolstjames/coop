use db_views::{
    post_view::PostView,
};
use serde::{Deserialize,Serialize};

#[derive(Deserialize, Debug)]
pub struct Login {
    pub email_handle_username: String,
    pub password: String,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Register {
    pub username: String,
    pub handle: String,
    pub email: Option<String>,
    pub password: String,
    pub password_verify: String,
}

#[derive(Serialize)]
pub struct LoginResponse {
  pub jwt: String,
}

