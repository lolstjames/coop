use db_schema::PostId;
use db_views::post_view::PostView;
use serde::{Deserialize, Serialize};
use url::Url;

#[derive(Deserialize, Debug)]
pub struct CreatePost {
    pub title: String,
    pub url: Option<Url>,
    pub body: Option<String>,
    pub client_id: Option<i32>,
    pub project_id: Option<i32>,
    pub deliverable_id: Option<i32>,
    pub assembly_id: Option<i32>,
    pub auth: String,
}

#[derive(Deserialize, Debug)]
pub struct GetPost {
    pub id: PostId,
    pub auth: Option<String>,
}

#[derive(Serialize)]
pub struct GetPostResponse{
    pub post_view: PostView,
    pub online: usize,
}

#[derive(Deserialize, Debug)]
pub struct GetPosts {
    pub type_: String,
    pub sort: String,
    pub page: Option<i64>,
    pub limit: Option<i64>,
    pub auth: Option<String>,
}

#[derive(Serialize, Debug)]
pub struct GetPostsResponse{
    pub posts: Vec<PostView>,
}

#[derive(Serialize, Clone)]
pub struct PostResponse {
    pub post_view: PostView,
}
