use crate::CRUD;
use bcrypt::{hash, DEFAULT_COST};
use diesel::{dsl::*, result::Error, *};
use db_schema::{
    naive_now,
    schema::accounts::dsl::*,
    bindings::account::{Account,AccountForm},
    AccountId,
};

mod safe_settings_type {
    use crate::ToSafeSettings;
    use db_schema::{schema::accounts::columns::*, bindings::account::Account};

    type Columns = (
        id,
        person_id,
        username,
        handle,
        no_contact,
        public_key,
        deleted,
        created,
        updated,
    );

    impl ToSafeSettings for Account {
        type SafeSettingsColumns = Columns;

        fn safe_settings_columns_tuple() -> Self::SafeSettingsColumns {
            (
                id,
                person_id,
                username,
                handle,
                no_contact,
                public_key,
                deleted,
                created,
                updated,
            )
        }
    }
}

pub trait Account_ {
    fn register(conn: &PgConnection, form: &AccountForm) -> Result<Account, Error>;
    fn update_password(
        conn: &PgConnection, 
        account_id: AccountId, 
        new_password: &str,
    ) -> Result<Account,Error>;
}

impl Account_ for Account {
    fn register(conn: &PgConnection, form: &AccountForm) -> Result<Account, Error>{
        let mut edited_account = form.clone();
        let password_hash = hash(&form.passhash, DEFAULT_COST).expect("Hashing Failed.");
        edited_account.passhash = password_hash;

        Self::create(&conn, &edited_account)
    }

    fn update_password(
        conn: &PgConnection, 
        account_id: AccountId, 
        new_password: &str,
    ) -> Result<Account,Error> {
        let password_hash = hash(new_password, DEFAULT_COST).expect("Hashing Failed.");

        diesel::update(accounts.find(account_id))
            .set((
                passhash.eq(password_hash),
                updated.eq(naive_now()),
        ))
        .get_result::<Self>(conn)
    }
}

impl CRUD<AccountForm, AccountId> for Account {
    fn create(c: &PgConnection, form: &AccountForm) -> Result<Self, Error> {
        insert_into(accounts)
            .values(form)
            .get_result::<Self>(c)
    }

    fn read(c: &PgConnection, account_id: AccountId) -> Result<Self, Error> {
        accounts.find(account_id).first::<Self>(c)
    }

    fn update(c:&PgConnection, account_id: AccountId, form: &AccountForm) -> Result<Self, Error> {
        diesel::update(accounts.find(account_id)).set(form).get_result::<Self>(c)
    }

    fn destroy(c: &PgConnection, account_id: AccountId) -> Result<usize, Error> {
        diesel::delete(accounts.find(account_id)).execute(c)
    }
}