use crate::{ CRUD, Favable, };
use diesel::{dsl::*, result::Error, *};
use db_schema::{
    bindings::post::{
        Post,
        PostForm,
        PostFav,
        PostFavForm,
    },
    PostId,
    AccountId,
    DbUrl,
    naive_now,
};

impl CRUD<PostForm, PostId> for Post {
    fn create(conn: &PgConnection, form: &PostForm) -> Result<Self, Error> {
        use db_schema::schema::posts::dsl::*;
        insert_into(posts).values(form).get_result::<Self>(conn)
    }

    fn read(conn: &PgConnection, post_id: PostId) -> Result<Self, Error> {
        use db_schema::schema::posts::dsl::*;
        posts.find(post_id).first::<Self>(conn)
    }

    fn update(conn: &PgConnection, post_id: PostId, form: &PostForm) -> Result<Self, Error> {
        use db_schema::schema::posts::dsl::*;
        diesel::update(posts.find(post_id))
            .set(form)
            .get_result::<Self>(conn)
    }

    fn destroy(conn: &PgConnection, post_id: PostId) -> Result<usize, Error> {
        use db_schema::schema::posts::dsl::*;
        diesel::delete(posts.find(post_id)).execute(conn)    
    }
}

// systemwide / scheduled task endpoints
pub trait Post_ {
    fn permanently_delete_for_creator(
        conn: &PgConnection,
        created_by: AccountId
    ) -> Result<Vec<Post>, Error>;
}

impl Post_ for Post {
    fn permanently_delete_for_creator(
        conn: &PgConnection, created_by: AccountId
    ) -> Result<Vec<Self>, Error> {
        use db_schema::schema::posts::dsl::*;

        let text_removed = "*Removed*";
        let url_removed = "https://news.yahoo.com";

        diesel::update(posts.filter(creator_id.eq(created_by)))
            .set((
                title.eq(text_removed),
                url.eq(url_removed),
                body.eq(text_removed),
                removed.eq(true),
                updated.eq(naive_now()),
            )).get_results::<Self>(conn)
    } 
}

impl Favable<PostFavForm, PostId> for PostFav {
    fn fav(conn:&PgConnection, form: &PostFavForm) -> Result<Self, Error> {
        use db_schema::schema::liked_posts::dsl::*;

        insert_into(liked_posts)
            .values(form)
            .on_conflict((post_id, account_id))
            .do_update()
            .set(form)
            .get_result::<Self>(conn)
    }
    
    fn unfav(conn:&PgConnection, account_id: AccountId, post_id: PostId)-> Result<usize, Error> {
        use db_schema::schema::liked_posts::dsl;

        diesel::delete(
            dsl::liked_posts
                .filter(dsl::post_id.eq(post_id))
                .filter(dsl::account_id.eq(account_id)),
        ).execute(conn)
    }
}