use crate::CRUD;
use diesel::{dsl::*, result::Error, *};
use db_schema::{
    naive_now,
    schema::persons::dsl::*,
    bindings::person::{Person, PersonForm},
    DbUrl,
    PersonId
};

mod safe_type {
    use crate::ToSafe;
    use db_schema::{schema::persons::columns::*, bindings::person::Person};

    type Columns = (
        id,
        name,
        bio,
        pronouns,
        phone,
        provider,
        street,
        city,
        state,
        zip,
        country,
        deleted,
        created,
    );

    impl ToSafe for Person {
        type SafeColumns = Columns;
        fn safe_columns_tuple() -> Self::SafeColumns {(
            id,
            name,
            bio,
            pronouns,
            phone,
            provider,
            street,
            city,
            state,
            zip,
            country,
            deleted,
            created,
        )}
    }
}

impl CRUD<PersonForm, PersonId> for Person {
    fn create(conn: &PgConnection, person_form: &PersonForm) -> Result<Self, Error> {
        insert_into(persons).values(person_form).get_result::<Self>(conn)
    }

    fn read(conn: &PgConnection, person_id: PersonId) -> Result<Self, Error> {
        persons
            .filter(deleted.eq(false))
            .find(person_id)
            .first::<Self>(conn)
    }

    fn update(conn: &PgConnection, person_id: PersonId, person_form: &PersonForm) -> Result<Self, Error> {
        diesel::update(persons.find(person_id))
            .set(person_form)
            .get_result::<Self>(conn)
    }

    fn destroy(conn: &PgConnection, person_id: PersonId) -> Result<usize, Error> {
        diesel::delete(persons.find(person_id)).execute(conn)
    }
}

pub trait Person_ {
//    fn ban_person(conn: &PgConnection, person_id: PersonId, ban: bool) -> Result<Person, Error>;
    fn find_by_name(conn: &PgConnection, name: &str) -> Result<Person, Error>;
    fn delete_account(conn: &PgConnection, person_id: PersonId) -> Result<Person, Error>;
}

impl Person_ for Person {
    // fn ban_person(conn: &PgConnection, person_id: PersonId, ban: bool) -> Result<Self, Error> {
    //     diesel::update(person.find(person_id))
    //         .set(banned.eq(ban))
    //         .get_result::<Self>(conn)
    // }
    
    fn find_by_name(conn: &PgConnection, from_name: &str) -> Result<Person, Error> {
        persons.filter(name.ilike(from_name)).first::<Person>(conn)
    }

    fn delete_account(conn: &PgConnection, person_id: PersonId) -> Result<Person, Error> {
        use db_schema::schema::accounts;

        diesel::update(accounts::table.filter(accounts::person_id.eq(person_id)))
            .set(accounts::person_id.eq(0))
            .execute(conn)?;
        
        diesel::update(persons.find(person_id))
            .set((
                name.eq("Deleted by user"),
                bio.eq::<Option<String>>(None),
                pronouns.eq::<Option<String>>(None),

                deleted.eq(true),
                updated.eq(naive_now()),
            ))
            .get_result::<Self>(conn)
    }
}