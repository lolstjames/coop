use diesel::{result::Error, *};
use db_schema::{DbUrl, AccountId};
use utils::ApiError;
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::{env, env::VarError};
use url::Url;

//pub mod aggregates;
pub mod bindings;

pub type DbPool = diesel::r2d2::Pool<diesel::r2d2::ConnectionManager<diesel::PgConnection>>;

pub trait CRUD<Form, IdType> {
    fn create(conn: &PgConnection, form: &Form) -> Result<Self, Error> where Self: Sized;
    fn read(conn: &PgConnection, id: IdType) -> Result<Self, Error> where Self: Sized;
    fn update(conn: &PgConnection, id: IdType, form: &Form) -> Result<Self, Error> where Self: Sized;
    fn destroy(_conn: &PgConnection, _id: IdType) -> Result<usize, Error>
        where Self: Sized, { unimplemented!() }
}

pub trait Favable<Form, IdType>{
    fn fav(conn: &PgConnection, form: &Form) -> Result<Self,Error> where Self: Sized;
    fn unfav(conn: &PgConnection, account_id: AccountId, item_id: IdType) -> Result<usize, Error> where Self: Sized;
}

pub trait MaybeOptional<T> {
    fn get_optional(self) -> Option<T>;
}

impl<T> MaybeOptional<T> for T{
    fn get_optional(self) -> Option<T> {
        Some(self)
    }
}

impl<T> MaybeOptional<T> for Option<T> {
    fn get_optional(self) -> Option<T> {
        self
    }
}

pub trait ToSafe {
    type SafeColumns;
    fn safe_columns_tuple() -> Self::SafeColumns;
}

pub trait ToSafeSettings {
    type SafeSettingsColumns;
    fn safe_settings_columns_tuple() -> Self::SafeSettingsColumns;
}

pub trait ViewToVec {
    type DbTuple;
    fn from_tuple_to_vec(tuple: Vec<Self::DbTuple>) -> Vec<Self>
        where Self: Sized;
}

pub fn get_database_url_from_env() -> Result<String, VarError> {
    env::var("LEMMY_DATABASE_URL")
}

pub fn diesel_option_overwrite(
    opt: &Option<String>
    ) -> Option<Option<String>> 
{
    match opt {
        Some(unwrapped) => {
            if !unwrapped.eq("") {
                Some(Some(unwrapped.to_owned()))
            } else {
                Some(None)
            }
        }
        None => None,
    }
}

pub fn diesel_option_overwrite_to_url(
    opt: &Option<String>
    ) -> Result<Option<Option<DbUrl>>, ApiError>
{
    match opt.as_ref().map(|s| s.as_str()){
        Some("") => Ok(Some(None)),
        Some(str_url) => match Url::parse(str_url) {
            Ok(url) => Ok(Some(Some(url.into()))),
            Err(_) => Err(ApiError::err("invalid_url")),
        },
        None => Ok(None),
    }
}


#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
