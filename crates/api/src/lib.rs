use api_deps::{
    post::*,
    account::*,
};
use utils::{ConnectionId, CoopError};
use websocket::{serialize_websocket_message, CoopContext, UserOperations};

use actix_web::{web, web::Data};
//use captcha::Captcha;
use serde::Deserialize;

pub mod post;
pub mod account;

#[async_trait::async_trait(?Send)]
pub trait Perform {
  type Response: serde::ser::Serialize + Send;

  async fn perform(
    &self,
    context: &Data<CoopContext>,
    websocket_id: Option<ConnectionId>,
  ) -> Result<Self::Response, CoopError>;
}

pub async fn match_websocket_operation(
    context: CoopContext,
    id: ConnectionId,
    op: UserOperations,
    data: &str,
) -> Result<String, CoopError> {

    match op {
    
        // Account Operations
        UserOperations::Register => do_websocket_operation::<Register>(context, id, op, data).await,

        // Post Operations
        UserOperations::CreatePost => {
            do_websocket_operation::<CreatePost>(context, id, op, data).await
        },

        UserOperations::GetPost => {
            do_websocket_operation::<GetPost>(context, id, op, data).await
        },
    
        // UserOperations::GetPosts => {
        //     do_websocket_operation::<GetPosts>(context, id, op, data).await
        // }
    
        _ => { panic!("unknown") }
    }
}

async fn do_websocket_operation<'a, 'b, Data>(
    context: CoopContext,
    id: ConnectionId,
    op: UserOperations,
    data: &str,
) -> Result<String, CoopError> 
where 
    for<'de> Data: Deserialize<'de> + 'a,
    Data: Perform,
{
    println!("{}", &data);
    let parsed_data: Data = serde_json::from_str(&data)?;
    let res = parsed_data
        .perform(&web::Data::new(context), Some(id))
        .await?;

    serialize_websocket_message(&op, &res)
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
