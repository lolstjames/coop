use crate::Perform;
use actix_web::web::Data;
use anyhow::Context;
use bcrypt::verify;
use chrono::Duration;
use api_deps::{
    blocking,
    get_account_view_from_jwt,
    password_length_check,
    account::*,
};
use db_queries::{
    diesel_option_overwrite,
    diesel_option_overwrite_to_url,
    bindings::{
        account::Account_,
        person::Person_,
        post::Post_,
    },
    CRUD,
};
use db_schema::{
    naive_now,
    bindings::{
        //password_reset_request::*,
        person::*,
        post::Post,
    }
};
use db_views::{
    account_view::AccountView,
};
use utils::{
    claims::Claims,
    settings::structs::Settings,
    utils::{
        generate_random_string,
        is_valid_display_name,
    },
    ApiError,
    ConnectionId,
    CoopError,
};
use websocket::{
    messages::{SendAllMessage,},
    CoopContext,
    UserOperations,
};

#[async_trait::async_trait(?Send)]
impl Perform for Login {
    type Response = LoginResponse;

    async fn perform(
        &self,
        context: &Data<CoopContext>,
        _websocket_id: Option<ConnectionId>,
    ) -> Result<LoginResponse, CoopError> {
        let data: &Login = &self;

        let username_or_email = data.email_handle_username.clone();
        let account_view = blocking(context.pool(), move |conn| {
            AccountView::find_by_email_or_name(conn, &username_or_email)
        })
        .await?
        .map_err(|_| ApiError::err("could_not_find_username_or_email"))?;

        let valid: bool = verify(
            &data.password,
            &account_view.account.passhash,
        )
        .unwrap_or(false);

        if !valid {
            return Err(ApiError::err("password_incorrect").into());
        }

        Ok(LoginResponse {
            jwt: Claims::jwt(account_view.account.id.0)?
        })


    }

}