use crate::Perform;
use actix_web::web::Data;
use api_deps::{blocking, password_length_check, account::*};
use db_queries::{
    bindings::{account::Account_},
    CRUD,
};
use db_schema::{
    bindings::{
        account::{Account, AccountForm},
        person::*
    },
};
use db_views::{
    account_view::AccountSettingsView,
};
use utils::{
    claims::Claims,
    settings::structs::Settings,
    ApiError,
    ConnectionId,
    CoopError,
};
use websocket::CoopContext;

#[async_trait::async_trait(?Send)]
impl Perform for Register {
    type Response = LoginResponse;

    async fn perform(
        &self,
        context: &Data<CoopContext>,
        _websocket_id: Option<ConnectionId>,
    ) -> Result<LoginResponse, CoopError> {
        let data: &Register = &self;
        
        password_length_check(&data.password)?;
        if data.password != data.password_verify {
            return Err(ApiError::err("password_match_fails").into());
        }

        let person_form = PersonForm {
            name: data.username.to_owned(),
            
            ..PersonForm::default()
        };

        let inserted_person = blocking(context.pool(),move |conn| {
            Person::create(conn, &person_form)
        })
        .await?
        .map_err(|_| ApiError::err("account_already_exists"))?;

        let account_form = AccountForm {
            person_id: inserted_person.id,
            username: data.username.to_owned(),
            handle: data.handle.to_owned(),
            passhash: data.password.to_owned(),
            
            ..AccountForm::default()
        };

        let inserted_account = match blocking(context.pool(), move |conn| {
            Account::register(conn, &account_form)
        })
        .await?
        {
            Ok(a) => a,
            Err(e) => {
                let err_type = if e.to_string() == 
                    "duplicate key value violates unique constraint \"account_user_email_key\"" {
                        "email_already_exists"
                    } else {
                        "user_already_exists"
                    };

                blocking(context.pool(), move |conn| {
                    Person::destroy(&conn, inserted_person.id)
                })
                .await??;

                return Err(ApiError::err(err_type).into());
            }
        };

        Ok(LoginResponse {
            jwt: Claims::jwt(inserted_account.id.0)?,
        })
    }
}