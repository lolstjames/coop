use crate::Perform;
use actix_web::web::Data;
use api_deps::{blocking, get_account_view_from_jwt, post::*};
use db_queries::{bindings::post::Post_, CRUD, Favable};
use db_schema::bindings::post::*;
use db_views::post_view::PostView;
use utils::{
    utils::{get_ip,},
    ConnectionId,
    ApiError,
    CoopError,
};
use websocket::{messages::SendPost, CoopContext, UserOperations};

#[async_trait::async_trait(?Send)]
impl Perform for CreatePost {
    type Response = PostResponse;

    async fn perform(
        &self,
        context: &Data<CoopContext>,
        websocket_id: Option<ConnectionId>,
    ) -> Result<PostResponse, CoopError> {
        let data: &CreatePost = &self;
        let account_view = get_account_view_from_jwt(&data.auth, context.pool()).await?;

        let data_url = data.url.as_ref();

        let post_form = PostForm {
            title: data.title.trim().to_owned(),
            url: data_url.map(|u| u.to_owned().into()),
            body: data.body.to_owned(),
            creator_id: account_view.account.id,
            ..PostForm::default()
        };


        let post_to_insert = 
            match blocking(context.pool(), move |conn| Post::create(conn, &post_form)).await? {
                Ok(post) => post,
                Err(e) => {
                    let err_type = if e.to_string() == "value too long for character varying(256)" {
                        "post_title_too_long"
                    } else {
                        "failed_to_create_post"
                    };

                    return Err(ApiError::err(err_type).into());
                }
            };
        
        let created_post_id = post_to_insert.id;
        
        let post_view = blocking(context.pool(), move |conn| {
            PostView::read(conn, created_post_id, Some(account_view.account.id))
        })
        .await?
        .map_err(|_| ApiError::err("couldnt_find_post"))?;

        let res = PostResponse { post_view };
        context.chat_server().do_send(SendPost {
            op: UserOperations::CreatePost,
            post: res.clone(),
            websocket_id,
        });

        Ok(res)
    }
}