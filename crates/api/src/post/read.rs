use crate::Perform;
use actix_web::web::Data;
use api_deps::{ blocking, get_account_view_from_jwt_opt, post::* };
use db_views::{
    post_view::{PostView,}
};
use utils::{ApiError,ConnectionId,CoopError};
use websocket::{messages::GetPostUsersOnline, CoopContext};
use std::str::FromStr;

#[async_trait::async_trait(?Send)]
impl Perform for GetPost {
    type Response = GetPostResponse;

    async fn perform(
        &self, 
        context: &Data<CoopContext>, 
        websocket_id: Option<ConnectionId>
    ) -> Result<GetPostResponse, CoopError> {
        let data: &GetPost = &self;
        let account_view = get_account_view_from_jwt_opt(&data.auth, context.pool()).await?;
        let account_id = account_view.map(|u| u.account.id);

        let id = data.id;
        let post_view = blocking(context.pool(), move |conn| {
            PostView::read(conn,id, account_id)
        })
        .await?
        .map_err(|_| ApiError::err("couldnt_find_post"))?;

        let online = 11;

        // let online = context
        //     .chat_server()
        //     .send(GetPostUsersOnline {post_id: data.id})
        //     .await
        //     .unwrap_or(1);

        Ok(GetPostResponse {
            post_view,
            online,
          })
      
    }
}