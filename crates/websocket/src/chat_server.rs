use crate::{
    messages::*,
    serialize_websocket_message,
    CoopContext,
    OperationType,
    UserOperations,
};
use actix::prelude::*;
use anyhow::Context as acontext;
use api_deps::post::*;
use background_jobs::QueueHandle;
use db_schema::{AccountId, PostId};
use diesel::{
    r2d2::{ConnectionManager, Pool},
    PgConnection,
};
use rand::rngs::ThreadRng;
use reqwest::Client;
use serde::Serialize;
use serde_json::Value;
use std::{
    collections::{HashMap, HashSet},
    str::FromStr,
};
use tokio::macros::support::Pin;
use utils::{
    location_info, 
    rate_limit::RateLimit, 
    ApiError, 
    ConnectionId, 
    CoopError, 
    IpAddr,
};

type MessageHandlerType = fn(
    context: CoopContext,
    id: ConnectionId,
    op: UserOperations,
    data: &str,
) -> Pin<Box<dyn Future<Output = Result<String, CoopError>> + '_>>;

pub struct SessionInfo {
    pub addr: Recipient<WsMessage>,
    pub ip: IpAddr,
}

pub struct ChatServer {
    pub sessions: HashMap<ConnectionId, SessionInfo>,
    pub post_rooms: HashMap<PostId, HashSet<ConnectionId>>,
    // pub project_rooms: HashMap<ProjectId, HashSet<ConnectionId>>
    // pub (super) user_rooms: HashMap<UserId, HashSet<ConnectionId>>,
    pub(super) rng: ThreadRng,
    pub(super) pool: Pool<ConnectionManager<PgConnection>>,
    pub(super) rate_limiter: RateLimit,

    message_handler: MessageHandlerType,

    client: Client,
    // activity_queue: QueueHandle,
}

impl ChatServer {
    pub fn startup(
        pool: Pool<ConnectionManager<PgConnection>>,
        rate_limiter: RateLimit,
        message_handler: MessageHandlerType,
        client: Client,
        // activity_queue: QueueHandle,
    ) -> ChatServer {
        Self {
            sessions: HashMap::new(),
            post_rooms: HashMap::new(),
            rng: rand::thread_rng(),
            pool,
            rate_limiter,
            message_handler,
            client,
            // activity_queue,
        }
    }

    pub fn join_post_room(&mut self, post_id: PostId, id: ConnectionId) -> Result<(), CoopError> {
        for sessions in self.post_rooms.values_mut() {
            sessions.remove(&id);
        }

        if self.post_rooms.get_mut(&post_id).is_none() {
            self.post_rooms.insert(post_id, HashSet::new());
        }

        self.post_rooms
            .get_mut(&post_id)
            .context(location_info!())?
            .insert(id);

        Ok(())
    }

    pub fn send_post<OP>(
        &self,
        user_operation: &OP,
        post_res: &PostResponse,
        websocket_id: Option<ConnectionId>,
    ) -> Result<(), CoopError>
    where
        OP: OperationType + ToString
    {
        let mut post_sent = post_res.clone();
        //post_sent.post_view.my_vote = None;

        self.send_post_room_message(user_operation, &post_sent, post_res.post_view.post.id, websocket_id)?;

        Ok(())
    }

    pub fn send_post_room_message<OP, Response>(
        &self,
        op: &OP,
        response: &Response,
        post_id: PostId,
        websocket_id: Option<ConnectionId>,
    ) -> Result<(), CoopError>
    where
        OP: OperationType + ToString,
        Response: Serialize,
    {
        let res_str = &serialize_websocket_message(op, response)?;
        if let Some(sessions) = self.post_rooms.get(&post_id) {
            for id in sessions {
                if let Some(my_id) = websocket_id {
                    if *id == my_id {
                        continue;
                    }
                }
                self.sendit(res_str, *id);
            }
        }
        Ok(())
    }

    fn sendit(&self, message: &str, id: ConnectionId) {
        if let Some(info) = self.sessions.get(&id) {
            let _ = info.addr.do_send(WsMessage(message.to_owned()));
        }
    }

    pub fn send_all_message<OP, Response>(
        &self,
        op: &OP,
        response: &Response,
        websocket_id: Option<ConnectionId>,
    ) -> Result<(), CoopError>
    where
        OP: OperationType + ToString,
        Response: Serialize,
    {
        let res_str = &serialize_websocket_message(op, response)?;
        for id in self.sessions.keys() {
            if let Some(my_id) = websocket_id {
                if *id == my_id {
                    continue;
                }
            }
            self.sendit(res_str, *id);
        }
        Ok(())
    }

    pub fn parse_json_message(
        &mut self,
        msg: StandardMessage,
        ctx: &mut Context<Self>,
    ) -> impl Future<Output = Result<String, CoopError>> {
        
        let rate_limiter = self.rate_limiter.clone();

        let ip: IpAddr = match self.sessions.get(&msg.id) {
            Some(info) => info.ip.to_owned(),
            None => IpAddr("no_ip".to_string()),
        };

        let context = CoopContext {
            pool: self.pool.clone(),
            chat_server: ctx.address(),
            client: self.client.to_owned(),
            // activity_queue: self.activity_queue.to_owned(),
        };

        let message_handler = self.message_handler;

        async move {
            let json: Value = serde_json::from_str(&msg.msg)?;
            let data = &json["data"].to_string();
            let op = &json["op"].as_str().ok_or(ApiError {
                message: "Unknown Operation Type".to_string(),
            })?;

            let user_operation = UserOperations::from_str(&op)?;
            let fut = (message_handler)(context, msg.id, user_operation.clone(), data);
            rate_limiter.message().wrap(ip, fut).await
        }
    }
}
