#[macro_use]
extern crate strum_macros;

use crate::chat_server::ChatServer;
use actix::Addr;
use background_jobs::QueueHandle;
use db_queries::DbPool;
use reqwest::Client;
use serde::Serialize;
use utils::CoopError;

pub mod chat_server;
pub mod handlers;
pub mod messages;
pub mod routes;


pub struct CoopContext {
    pub pool: DbPool,
    pub client: Client,
    pub chat_server: Addr<ChatServer>,
    // pub activity_queue: QueueHandle,
}

impl CoopContext {
    pub fn create(
        // activity_queue: QueueHandle,
        chat_server: Addr<ChatServer>,
        client: Client,
        pool: DbPool,
    ) -> CoopContext {
        CoopContext {
            // activity_queue,
            chat_server,
            client,
            pool,
        }
    }

    // pub fn activity_queue(&self) -> &QueueHandle {
    //     &self.activity_queue
    // }
    pub fn chat_server(&self) -> &Addr<ChatServer> {
        &self.chat_server
    }
    pub fn client(&self) -> &Client {
        &self.client
    }
    pub fn pool(&self) -> &DbPool {
        &self.pool
    }
}

impl Clone for CoopContext {
    fn clone(&self) -> Self {
        CoopContext {
            // activity_queue: self.activity_queue.clone(),
            chat_server: self.chat_server.clone(),
            client: self.client.clone(),
            pool: self.pool.clone(),
        }
    }
}

#[derive(Serialize)]
struct WebsocketResponse<T> {
    op: String,
    data: T,
}

pub fn serialize_websocket_message<OP, Response>(
    op: &OP,
    data: &Response,
) -> Result<String, CoopError>
where
    Response: Serialize,
    OP: ToString,
{
    let response = WebsocketResponse {
        op: op.to_string(),
        data,
    };
    Ok(serde_json::to_string(&response)?)
}

#[derive(EnumString, ToString, Debug, Clone)]
pub enum UserOperations {
    Login,
    GetCaptcha,
    Register,
    CreatePost,
    GetPost,
    GetPosts,
    EditPost,
    DeletePost,
    RemovePost,
    SendPost,
}


pub trait OperationType {}

impl OperationType for UserOperations {}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
