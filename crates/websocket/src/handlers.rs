use crate::{
    chat_server::{ChatServer, SessionInfo},
    messages::*,
    OperationType,
};
use actix::{Actor, Context, Handler, ResponseFuture};
use db_schema::naive_now;
use log::{error, info};
use rand::Rng;
use serde::Serialize;
use utils::ConnectionId;

impl Actor for ChatServer {
    type Context = Context<Self>;
}

impl Handler<Connect> for ChatServer {
    type Result = ConnectionId;

    fn handle(&mut self, msg: Connect, _ctx: &mut Context<Self>) -> Self::Result {
        let id = self.rng.gen::<usize>();
        info!("{} joined", &msg.ip);

        self.sessions.insert(
            id,
            SessionInfo {
                addr: msg.addr,
                ip: msg.ip,
            },
        );
        id
    }
}

impl Handler<Disconnect> for ChatServer {
    type Result = ();

    fn handle(&mut self, msg: Disconnect, _: &mut Context<Self>) {
        if self.sessions.remove(&msg.id).is_some() {
            for sessions in self.post_rooms.values_mut() {
                sessions.remove(&msg.id);
            }
        }
    }
}

impl Handler<StandardMessage> for ChatServer {
    type Result = ResponseFuture<Result<String, std::convert::Infallible>>;

    fn handle(&mut self, msg: StandardMessage, ctx: &mut Context<Self>) -> Self::Result {
        let fut = self.parse_json_message(msg, ctx);

        Box::pin(async move {
            match fut.await {
                Ok(m) => Ok(m),
                Err(e) => {
                    error!("Error during message handling {}", e);
                    Ok(e.to_string())
                }
            }
        })
    }
}

impl <OP, Response> Handler<SendAllMessage<OP,Response>> for ChatServer
where
    OP: OperationType + ToString,
    Response: Serialize
{
    type Result = ();

    fn handle(&mut self, msg: SendAllMessage<OP,Response>, _: &mut Context<Self>) {
        self
            .send_all_message(&msg.op, &msg.response, msg.websocket_id)
            .ok();
    }
}


impl<OP> Handler<SendPost<OP>> for ChatServer
where 
    OP: OperationType + ToString,
{
    type Result = ();
    
    fn handle(&mut self, msg: SendPost<OP>, _: &mut Context<Self>) {
        self.send_post(&msg.op, &msg.post, msg.websocket_id).ok(); 
    }
}