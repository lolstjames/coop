use crate::UserOperations;
use actix::{prelude::*, Recipient};
use api_deps::post::PostResponse;
use db_schema::{AccountId, PostId};
use serde::{Deserialize, Serialize};
use utils::{ConnectionId, IpAddr};

#[derive(Message)]
#[rtype(result = "()")]
pub struct WsMessage(pub String);

#[derive(Message)]
#[rtype(usize)]
pub struct Connect {
    pub addr: Recipient<WsMessage>,
    pub ip: IpAddr,
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct Disconnect {
    pub id: ConnectionId,
    pub ip: IpAddr,
}

#[derive(Serialize, Deserialize, Message)]
#[rtype(result = "Result<String, std::convert::Infallible>")]
pub struct StandardMessage {
    pub id: ConnectionId,
    pub msg: String,
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct JoinPostRoom {
    pub post_id: PostId,
    pub id: ConnectionId,
}

#[derive(Message)]
#[rtype(usize)]
pub struct GetUsersOnline;

#[derive(Message)]
#[rtype(usize)]
pub struct GetPostUsersOnline {
    pub post_id: PostId,
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct SendAllMessage<OP: ToString, Response> {
    pub op: OP,
    pub response: Response,
    pub websocket_id: Option<ConnectionId>,
}
#[derive(Message)]
#[rtype(result = "()")]
pub struct SendPost<OP: ToString> {
    pub op: OP,
    pub post: PostResponse,
    pub websocket_id: Option<ConnectionId>,
}

// #[derive(Serialize, Clone)]
// pub struct CommentResponse {
//     pub comment_view: CommentView,
//     pub recipient_ids: Vec<AccountId>,
//     pub form_id: Option<String>, // An optional front end ID, to tell which is coming back
// }

// #[derive(Message)]
// #[rtype(result = "()")]
// pub struct SendComment<OP: ToString> {
//   pub op: OP,
//     pub comment: CommentResponse,
//     pub websocket_id: Option<ConnectionId>,
// }


#[derive(Message)]
#[rtype(result = "()")]
pub struct JoinUserRoom {
    pub local_user_id: AccountId,
    pub id: ConnectionId,
}

#[derive(Message, Debug)]
#[rtype(result = "()")]
pub struct CaptchaItem {
    pub uuid: String,
    pub answer: String,
    pub expires: chrono::NaiveDateTime,
}

#[derive(Message)]
#[rtype(bool)]
pub struct CheckCaptcha {
    pub uuid: String,
    pub answer: String,
}
