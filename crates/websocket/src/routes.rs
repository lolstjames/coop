use crate::{
    chat_server::ChatServer,
    messages::{Connect, Disconnect, StandardMessage, WsMessage},
    CoopContext,
};
use actix::prelude::*;
use actix_web::*;
use actix_web_actors::ws;
use log::{debug, error, info};
use std::time::{Duration, Instant};
use utils::{utils::get_ip, ConnectionId, IpAddr};

const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(10);
const CLIENT_TIMEOUT: Duration = Duration::from_secs(20);

pub async fn chat_route(
    req: HttpRequest,
    rwstream: web::Payload,
    context: web::Data<CoopContext>,
) -> Result<HttpResponse, Error> {
    ws::start(
        WebsocketSession {
            cs_addr: context.chat_server().to_owned(),
            id: 0,
            hb: Instant::now(),
            ip: get_ip(&req.connection_info()),
        },
        &req,
        rwstream,
    )
}

struct WebsocketSession {
    cs_addr: Addr<ChatServer>,
    id: ConnectionId,
    ip: IpAddr,
    hb: Instant,
}

impl Actor for WebsocketSession {
    type Context = ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.hb(ctx);

        self.cs_addr
            .send(Connect {
                addr: ctx.address().recipient(),
                ip: self.ip.to_owned(),
            })
            .into_actor(self)
            .then(|res, act, ctx| {
                match res {
                    Ok(res) => act.id = res,
                    _ => ctx.stop(),
                }
                actix::fut::ready(())
            })
            .wait(ctx);
    }

    fn stopping(&mut self, _ctx: &mut Self::Context) -> Running {
        self.cs_addr.do_send(Disconnect {
            id: self.id,
            ip: self.ip.to_owned(),
        });
        Running::Stop
    }
}

impl Handler<WsMessage> for WebsocketSession {
    type Result = ();

    fn handle(&mut self, msg: WsMessage, ctx: &mut Self::Context) {
        ctx.text(msg.0)
    }
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for WebsocketSession {
    fn handle(&mut self, result: Result<ws::Message, ws::ProtocolError>, ctx: &mut Self::Context) {
        let message = match result {
            Ok(m) => m,
            Err(e) => {
                error!("{}", e);
                return;
            }
        };
        match message {
            ws::Message::Ping(msg) => {
                self.hb = Instant::now();
                ctx.pong(&msg);
            }
            ws::Message::Pong(_) => {
                self.hb = Instant::now();
            }
            ws::Message::Text(text) => {
                let m = text.trim().to_owned();
                info!("SMSlol Recieved: {:?} from {:?}", &m, self.id);

                self.cs_addr
                    .send(StandardMessage {
                        msg: m,
                        id: self.id,
                    })
                    .into_actor(self)
                    .then(|res, _, ctx| {
                        match res {
                            Ok(Ok(res)) => ctx.text(res),
                            Ok(Err(_)) => {}
                            Err(e) => error!("{}", &e),
                        }
                        actix::fut::ready(())
                    })
                    .spawn(ctx);
            }
            ws::Message::Binary(_bin) => info!("Unanticipated Binary"),
            ws::Message::Close(_) => {
                ctx.stop();
            }
            _ => {}
        }
    }
}

impl WebsocketSession {
    fn hb(&self, ctx: &mut ws::WebsocketContext<Self>) {
        ctx.run_interval(HEARTBEAT_INTERVAL, |act, ctx| {
            if Instant::now().duration_since(act.hb) > CLIENT_TIMEOUT {
                debug!("Client heartbeat failed, disconnecting");

                act.cs_addr.do_send(Disconnect {
                    id: act.id,
                    ip: act.ip.to_owned(),
                });
                ctx.stop();
                return;
            }
            ctx.ping(b"");
        });
    }
}
