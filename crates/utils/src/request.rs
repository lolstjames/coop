use crate::{settings::structs::Settings, CoopError};
use anyhow::anyhow;
use log::error;
use percent_encoding::{utf8_percent_encode, NON_ALPHANUMERIC};
use reqwest::Client;
use serde::Deserialize;
use std::future::Future;
use thiserror::Error;
use url::Url;

#[derive(Clone, Debug, Error)]
#[error("Error sending request, {0}")]
struct SendError(pub String);

#[derive(Clone, Debug, Error)]
#[error("Error receiving response, {0}")]
pub struct RecvError(pub String);

pub async fn retry<F, Fut, T>(f: F) -> Result<T, reqwest::Error>
where
    F: Fn() -> Fut,
    Fut: Future<Output = Result<T, reqwest::Error>>,
{
    retry_custom(|| async { Ok((f)().await) }).await
}

async fn retry_custom<F, Fut, T>(f: F) -> Result<T, reqwest::Error>
where
    F: Fn() -> Fut,
    Fut: Future<Output = Result<Result<T, reqwest::Error>, reqwest::Error>>,
{
    let mut response: Option<Result<T, reqwest::Error>> = None;
    for _ in 0u8..3 {
        match (f)().await? {
            Ok(t) => return Ok(t),
            Err(e) => {
                if e.is_timeout() {
                    response = Some(Err(e));
                    continue;
                }
                return Err(e);
            }
        }
    }
    response.expect("retry http request")
}

// This is for backend calls to external resources including image hosing, code snippets et cetera

async fn is_image_content_type(client: &Client, test: &Url) -> Result<(), CoopError> {
    let response = retry(|| client.get(test.to_owned()).send()).await?;
    if response
        .headers()
        .get("Content-Type")
        .ok_or_else(|| anyhow!("No Content-Type header"))?
        .to_str()?
        .starts_with("image/")
    {
        Ok(())
    } else {
        Err(anyhow!("Not an image type.").into())
    }
}
