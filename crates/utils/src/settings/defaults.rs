use crate::settings::{CaptchaConfig, DatabaseConfig, RateLimitConfig, Settings};
use std::net::{IpAddr, Ipv4Addr};

impl Default for Settings {
    fn default() -> Self {
        Self {
            database: Some(DatabaseConfig::default()),
            rate_limit: Some(RateLimitConfig::default()),
            captcha: Some(CaptchaConfig::default()),
            email: None,
            // setup: None,
            hostname: None,
            bind: Some(IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0))),
            port: Some(42069),
            tls_enabled: Some(true),
            jwt_secret: Some("cha3Jie6oud4Ui0y".into()),
        }
    }
}

impl Default for DatabaseConfig {
    fn default() -> Self {
        Self {
            user: Some("coop".to_string()),
            password: "password".into(),
            host: "localhost".into(),
            port: Some(5432),
            database: Some("coop".to_string()),
            pool_size: Some(5),
        }
    }
}

impl Default for CaptchaConfig {
    fn default() -> Self {
        Self {
            enabled: true,
            difficulty: "medium".into(),
        }
    }
}

impl Default for RateLimitConfig {
    fn default() -> Self {
        Self {
            message: 180,
            message_per_second: 60,
            post: 6,
            post_per_second: 600,
            register: 3,
            register_per_second: 3600,
            image: 6,
            image_per_second: 3600,
        }
    }
}
