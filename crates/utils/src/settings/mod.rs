use crate::{
    location_info,
    settings::structs::{
        CaptchaConfig,
        DatabaseConfig,
        EmailConfig,
        RateLimitConfig,
        Settings,
        // SetupConfig,
    },
    CoopError,
};
use anyhow::{anyhow, Context};
use merge::Merge;
use std::{env, fs, io::Error, net::IpAddr, sync::RwLock};
use toml::from_str;

pub mod defaults;
pub mod structs;

static CONFIG_FILE: &str = "config/config.toml";

lazy_static! {
    static ref SETTINGS: RwLock<Settings> =
        RwLock::new(Settings::init().expect("Failed to load config.toml"));
}

impl Settings {
    fn init() -> Result<Self, CoopError> {
        let mut custom_config = from_str::<Settings>(&Self::read_config_file()?)?;

        custom_config.merge(envy::prefixed("COOP_").from_env::<Settings>()?);

        custom_config.merge(Settings::default());

        if custom_config.hostname == Settings::default().hostname {
            return Err(anyhow!("Hostname setting missing!").into());
        }

        Ok(custom_config)
    }

    pub fn get_database_url(&self) -> String {
        let conf = self.database();
        format!(
            "postgres://{}:{}@{}:{}/{}",
            conf.user(),
            conf.password,
            conf.host,
            conf.port(),
            conf.database(),
        )
    }

    pub fn get() -> Self {
        SETTINGS.read().expect("read settings").to_owned()
    }
    pub fn get_config_location() -> String {
        env::var("COOP_CONFIG_FILE").unwrap_or_else(|_| CONFIG_FILE.to_string())
    }
    pub fn read_config_file() -> Result<String, Error> {
        fs::read_to_string(Self::get_config_location())
    }
    pub fn get_protocol_string(&self) -> &'static str {
        if let Some(tls_enabled) = self.tls_enabled {
            if tls_enabled {
                "https"
            } else {
                "http"
            }
        } else {
            "http"
        }
    }
    pub fn get_protocol_and_hostname(&self) -> String {
        format!("{}://{}", self.get_protocol_string(), self.hostname())
    }

    pub fn get_hostname_without_port(&self) -> Result<String, anyhow::Error> {
        Ok(self
            .hostname()
            .split(':')
            .collect::<Vec<&str>>()
            .first()
            .context(location_info!())?
            .to_string())
    }
    
    pub fn save_config_file(data: &str) -> Result<String, CoopError> {
        fs::write(CONFIG_FILE, data)?;

        // Reload the new settings
        // From https://stackoverflow.com/questions/29654927/how-do-i-assign-a-string-to-a-mutable-static-variable/47181804#47181804
        let mut new_settings = SETTINGS.write().expect("write config");
        *new_settings = match Settings::init() {
            Ok(c) => c,
            Err(e) => panic!("{}", e),
        };

        Ok(Self::read_config_file()?)
    }

    pub fn database(&self) -> DatabaseConfig {
        self.database.to_owned().unwrap_or_default()
    }
    pub fn hostname(&self) -> String {
        self.hostname.to_owned().unwrap_or_default()
    }
    pub fn bind(&self) -> IpAddr {
        self.bind.expect("return bind address")
    }
    pub fn port(&self) -> u16 {
        self.port.unwrap_or_default()
    }
    pub fn tls_enabled(&self) -> bool {
        self.tls_enabled.unwrap_or_default()
    }
    pub fn jwt_secret(&self) -> String {
        self.jwt_secret.to_owned().unwrap_or_default()
    }
    pub fn rate_limit(&self) -> RateLimitConfig {
        self.rate_limit.to_owned().unwrap_or_default()
    }
    pub fn captcha(&self) -> CaptchaConfig {
        self.captcha.to_owned().unwrap_or_default()
    }
    pub fn email(&self) -> Option<EmailConfig> {
        self.email.to_owned()
    }
    // pub fn setup(&self) -> Option<SetupConfig> {
    //     self.setup.to_owned()
    // }
}
