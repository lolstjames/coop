use crate::{settings::structs::Settings, ApiError, IpAddr};
use rand::{distributions::Alphanumeric, thread_rng, Rng};
use chrono::{DateTime, FixedOffset, NaiveDateTime};
use actix_web::dev::ConnectionInfo;

pub fn get_ip(conn_info: &ConnectionInfo) -> IpAddr {
    IpAddr(
        conn_info
            .realip_remote_addr()
            .unwrap_or("127.0.0.1:6969")
            .split(":")
            .next()
            .unwrap_or("127.0.0.1")
            .to_string(),
    )
}

pub fn generate_random_string() -> String {
    thread_rng()
        .sample_iter(&Alphanumeric)
        .map(char::from)
        .take(30)
        .collect()
}

pub fn is_valid_display_name(name: &str) -> bool {
    !name.starts_with('@')
    && !name.starts_with('\u{200b}')
    && name.chars().count() >= 3
    && name.chars().count() <= 31
}

pub fn markdown_to_html(text: &str) -> String {
    comrak::markdown_to_html(text, &comrak::ComrakOptions::default())
}
pub fn naive_from_unix(time: i64) -> NaiveDateTime {
    NaiveDateTime::from_timestamp(time, 0)
}