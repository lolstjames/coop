#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate strum_macros;

pub mod claims;
pub mod email;
pub mod rate_limit;
pub mod request;
pub mod settings;
pub mod utils;
pub mod version;

#[cfg(test)]
mod test;

use http::StatusCode;
use std::fmt;
use thiserror::Error;

#[macro_export]
macro_rules! location_info {
    () => {
        format!(
            "None value at {}:{}, column {}",
            file!(),
            line!(),
            column!()
        )
    };
}

pub type ConnectionId = usize;

#[derive(PartialEq, Eq, Hash, Debug, Clone)]
pub struct IpAddr(pub String);

impl fmt::Display for IpAddr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[derive(Debug, Error)]
#[error("{{\"error\":\"{message}\"}}")]
pub struct ApiError {
    pub message: String,
}

impl ApiError {
    pub fn err(msg: &str) -> Self {
        Self {
            message: msg.to_string(),
        }
    }
}

#[derive(Debug)]
pub struct CoopError {
    pub inner: anyhow::Error,
}

impl<T> From<T> for CoopError
where
    T: Into<anyhow::Error>,
{
    fn from(t: T) -> Self {
        CoopError { inner: t.into() }
    }
}

impl std::fmt::Display for CoopError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        self.inner.fmt(f)
    }
}

impl actix_web::error::ResponseError for CoopError {
    fn status_code(&self) -> StatusCode {
        match self.inner.downcast_ref::<diesel::result::Error>() {
            Some(diesel::result::Error::NotFound) => StatusCode::NOT_FOUND,
            _ => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}
