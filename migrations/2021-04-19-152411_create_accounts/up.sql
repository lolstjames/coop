-- Your SQL goes here
create table accounts (
    id serial primary key,

    person_id int references persons on update cascade on delete cascade not null,
    
    username varchar(63) not null,
    passhash text not null,
    handle varchar(63) not null,

    password_reset varchar(127),
    password_reset_expires timestamp,

    public_key text,
    private_key text,

    no_contact boolean,

    logins smallint,
    last_login timestamp,

    deleted boolean default false,

    created timestamp not null default now(),
    updated timestamp
);
