-- Your SQL goes here
create table deliverables (
    id serial primary key,

    title varchar(255) not null,
    description text,

    project_id int references projects on update cascade on delete cascade not null,

    deleted boolean default false,
    visibility int,

    creator_id int references accounts,
    created timestamp not null default now(),
    updated timestamp
);