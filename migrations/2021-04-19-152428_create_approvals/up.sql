-- Your SQL goes here

-- Yo what the fuck is this?
-- lol oh i remember
create table approvals (
    id serial primary key,

    value int,
    account_id int references accounts not null,

    assembly_id int,
    client_id int,
    project_id int,
    deliverable_id int,
    proposal_id int,

    created timestamp not null default now(),
    updated timestamp
);