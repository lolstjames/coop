-- Your SQL goes here
create table clients (
    id serial primary key,

    name varchar(255) not null,
    logo bytea,
    
    contact_name varchar(255),
    contact_email varchar(254),
    contact_phone varchar(31),
    contact_provider varchar(63),

    phone varchar(31),
    email varchar(254),
    street varchar(127),
    city varchar(63),
    state varchar(63),
    country char(4),
    zip varchar(15),

    creator_id int references accounts,

    created timestamp not null default now(),
    updated timestamp
);