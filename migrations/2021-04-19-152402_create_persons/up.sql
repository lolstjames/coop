-- Your SQL goes here
create table persons (
    id serial primary key,

    name varchar(63) not null,
    email varchar(254) not null,
    bio text,
    pronouns varchar(63),

    phone varchar(31),
    provider varchar(63),

    street varchar(127),
    city varchar(63),
    state varchar(63),
    country char(4),
    zip varchar(15),
    
    deleted boolean,

    created timestamp not null default now(),
    updated timestamp
);
