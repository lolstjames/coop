-- Your SQL goes here
create table posts (
    id serial primary key,
    
    title varchar(255) not null,
    url text,
    body text,

    creator_id int references accounts on update cascade on delete cascade not null,

    -- everything is a post, and every post has a parent
    project_id int references projects on update cascade on delete cascade,
    client_id int references clients on update cascade on delete cascade,
    deliverable_id int references deliverables on update cascade on delete cascade,
    assembly_id int references assemblies on update cascade on delete cascade,

    -- combined with permissions, determines security levels
    visibility int,

    removed boolean default false not null,
    locked boolean default false not null,

    created timestamp not null default now(),
    updated timestamp
);

create table liked_posts (
    id serial primary key,

    post_id int references posts on update cascade on delete cascade not null,
    account_id int references accounts on update cascade on delete cascade not null,
    score smallint not null,
    
    created timestamp not null default now(),
    updated timestamp,
    unique(post_id, account_id)
);

create table saved_posts (
    id serial primary key,

    post_id int references posts on update cascade on delete cascade not null,
    account_id int references accounts on update cascade on delete cascade not null,
    
    created timestamp not null default now(),
    updated timestamp,
    unique(post_id, account_id)
);

create table read_posts (
    id serial primary key,

    post_id int references posts on update cascade on delete cascade not null,
    account_id int references accounts on update cascade on delete cascade not null,
    
    created timestamp not null default now(),
    updated timestamp,
    unique(post_id, account_id)
);
