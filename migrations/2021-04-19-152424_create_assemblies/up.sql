-- Your SQL goes here
create table assemblies (
    id serial primary key,

    name varchar(127) not null,
    domain text not null,
    
    proposed_by int not null,
    
    created timestamp not null default now(),
    updated timestamp
);