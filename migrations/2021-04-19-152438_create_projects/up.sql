-- Your SQL goes here
create table projects (
    id serial primary key,

    title varchar(255) not null,
    description text,

    client_id int references clients on update cascade on delete cascade not null,

    deleted boolean default false,
    visibility int,

    creator_id int references accounts,
    created timestamp not null default now(),
    updated timestamp
);