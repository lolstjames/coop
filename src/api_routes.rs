use actix_web::{error::ErrorBadRequest, *};
use utils::rate_limit::RateLimit;
use api::Perform;
use api_deps::{post::*, account::*};
use websocket::{routes::chat_route, CoopContext};
use serde::Deserialize;

pub fn config(cfg: &mut web::ServiceConfig, rate_limit: &RateLimit){

    cfg.service(
        web::scope("/api/v1")
        
        // websocket
        .service(web::resource("/ws").to(chat_route))

        // accounts
        .service(
            web::resource("/account/register")
                .guard(guard::Post())
                .wrap(rate_limit.register())
                .route(web::post().to(route_post_crud::<Register>)),
        )
        .service(
            web::scope("/account")
                .wrap(rate_limit.message())
                .route("/login", web::post().to(route_post::<Login>))
        )

        // we're posting
        .service(
            web::scope("/post")
                .wrap(rate_limit.message())
                .route("", web::post().to(route_post_crud::<CreatePost>))
                .route("", web::get().to(route_get_crud::<GetPost>))
        )
    );
}

async fn perform<Request>(
    data: Request,
    context: web::Data<CoopContext>
) -> Result<HttpResponse, Error>
where
    Request: Perform,
    Request: Send + 'static,
{
    let res = data
        .perform(&context, None)
        .await
        .map(|json| HttpResponse::Ok().json(json))
        .map_err(ErrorBadRequest)?;

    Ok(res)
}

async fn route_get<'a, Data>(
    data: web::Query<Data>,
    context: web::Data<CoopContext>,
) -> Result<HttpResponse, Error>
where
    Data: Deserialize<'a> + Send + 'static + Perform,
{
    perform::<Data>(data.0, context).await
}

async fn route_post<'a, Data>(
    data: web::Json<Data>,
    context: web::Data<CoopContext>,
) -> Result<HttpResponse, Error>
where
    Data: Deserialize<'a> + Send + 'static + Perform + std::fmt::Debug,
{
    dbg!("{:#?}", &data);

    perform::<Data>(data.0, context).await
}

async fn perform_crud<Request>(
    data: Request,
    context: web::Data<CoopContext>,
) -> Result<HttpResponse, Error>
where 
    Request: Perform, 
    Request: Send + 'static,
{

    let res = data
        .perform(&context, None)
        .await
        .map(|json| HttpResponse::Ok().json(json))
        .map_err(ErrorBadRequest)?;

    Ok(res)
}

async fn route_get_crud<'a, Data>(
    data: web::Query<Data>,
    context: web::Data<CoopContext>,
) -> Result<HttpResponse, Error> 
where 
    Data: Deserialize<'a> + Send + 'static + Perform + std::fmt::Debug,
{
    perform_crud::<Data>(data.0, context).await
}

async fn route_post_crud<'a, Data>(
    data: web::Json<Data>,
    context: web::Data<CoopContext>
) -> Result<HttpResponse, Error>
where
    Data: Deserialize<'a> + Send + 'static + Perform
{
    perform_crud::<Data>(data.0, context).await
}
