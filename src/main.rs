#[macro_use]
extern crate diesel_migrations;

use actix::prelude::*;
use actix_web::*;
use diesel::{
    r2d2::{ConnectionManager, Pool},
    PgConnection,
};
use api::match_websocket_operation;
use api_deps::blocking;
use db_queries::get_database_url_from_env;
use coop::{api_routes,};
use utils::{
    rate_limit::{rate_limiter::RateLimiter, RateLimit},
    settings::structs::Settings,
    CoopError,
};
use websocket::{chat_server::ChatServer, CoopContext};
use reqwest::Client;
use std::{sync::Arc, thread};
use tokio::sync::Mutex;


embed_migrations!();

#[actix_web::main]
async fn main() -> Result<(), CoopError>{

    env_logger::init();
    let settings = Settings::get();

    let db_url = match get_database_url_from_env() {
        Ok(url) => url,
        Err(_) => settings.get_database_url(),
    };
    let manager = ConnectionManager::<PgConnection>::new(&db_url);
    let pool = Pool::builder()
        .max_size(settings.database().pool_size().into())
        .build(manager)
        .unwrap_or_else(|_| panic!("Error connecting to database {}", db_url));

    let rate_limiter = RateLimit {
        rate_limiter: Arc::new(Mutex::new(RateLimiter::default())),
    };

    println!(
        "Starting http at {}:{}",
        settings.bind(),
        settings.port()
    );

    let chat_server = ChatServer::startup(
        pool.clone(), 
        rate_limiter.clone(), 
        |c, i, o, d| Box::pin(match_websocket_operation(c,i,o,d)), 
        Client::default(),
    ).start();

    HttpServer::new(move || {
        let context = CoopContext::create(chat_server.to_owned(), Client::default(), pool.clone());
        let rate_limiter = rate_limiter.clone();
        App::new()
            .wrap(middleware::Logger::default())
            .data(context)
            .configure(|cfg| api_routes::config(cfg, &rate_limiter))
    })
    .bind((settings.bind(), settings.port()))?
    .run()
    .await?;

    Ok(())
}
