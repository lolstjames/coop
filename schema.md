# cooperative.studio

an initial and not at all comprehensive attempt at creating a data scheme for a web app
powering a cooperative, employee owned, assembly governed branding, creative, and
narrative agency.

## Technologies

 - postgresql
 - diesel && diesel_uli
 - redis - probably eventually but hopefully never

## tables

 - charter - this one is special - can only be updated by democratic process
 - charter_revisions - in which we `'"blockchain"'` the governing document of the
   cooperative. in theory, this would prevent the coop from ever being sold without 100%
   of the worker-owners voting to sell. 
 - persons - individual people with individual people stuff
 - accounts - username, password, jwt and other security stuff
 - assemblies - working groups that determine the things that will be put to vote
 - clients - organizations
 - posts - everything is a post cause we're always posting - never log off
    - lots of different kinds
    - internal
    - client facing
    - assembly discussion
    - assembly nominations and votes
 - permissions
    - a way of managing user types and access without having different account types
 - invoices
